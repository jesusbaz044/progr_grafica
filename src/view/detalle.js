import React from 'react';
import { StyleSheet, Text, View, Button, Image, ScrollView, TouchableOpacity} from 'react-native';
import Home from '../view/home' ;
export default function HomeScreen({route}){
  const {peliId} = route.params;  
  return(
    <ScrollView>
       <View style={styles.detalle}>
         <Text style={styles.title}>{peliId.title}</Text>
         <Image style={styles.image} source={{uri: peliId.cover}}></Image>
         <Text style={styles.descripcion}>{peliId.todo}</Text>
         <TouchableOpacity style={styles.boton}>
           <Text style={styles.letra}>Comprar</Text>
         </TouchableOpacity>
      
      
    </View>
 
    </ScrollView>
 )
}
const styles = StyleSheet.create({
  detalle: {
     backgroundColor: '#e84393',
     height: 'auto'
  },
  title: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold'

  },
  descripcion:{
    color: '#fff',
    textAlign: 'justify',
    fontSize: 14,
    margin: 10
  },
  boton: {
    alignSelf: 'center',
    alignItems: 'center',
    padding: 20,
    width: 200,
    marginTop: 5,
    marginBottom: 55,
    borderColor:"#fff",
    borderWidth:1,
  },
  letra: {
    color: '#fff',
    fontWeight: 'bold'
  },
  image: {
    width: 300,
    height: 300,
    marginTop: 10,
    resizeMode: 'cover',
    alignSelf: 'center',
    marginBottom: 10,
    borderWidth: 3,
    borderColor: '#fff'
  }
})