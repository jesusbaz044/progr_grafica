import React, {Component, useState, useEffect} from 'react';
import { View, StyleSheet, Text, Button, TouchableHighlight, Image, TextInput, Alert } from 'react-native';
import Icon from 'react-native-ionicons'
class Form extends React.Component {
  constructor(){
    super()
    this.state={
      usuario:'',
      contraseña:''
    }
  }
  changeUsuario(usuario){
    this.setState({usuario})
  }
  changeContraseña(contraseña){
    this.setState({contraseña})
  }
  botonPress(){
    if(this.state.usuario && this.state.contraseña){
      this.props.navigation.navigate('Home')
    }
    else{
      alert("Rellene todos los campos!")
    }
    
  }
  boton(){
    this.props.navigation.navigate('Form')
  }
  render() {
    return (
      <View style={styles.container}>
                  <Image
        style={{ width: 415, height: 236, alignSelf: 'center'}}
          source={require("../assets/bienvenido.gif")}
        />
        <TextInput
        placeholder="Usuario"
        style={styles.TextInput}
        underlineColorAndroid={'transparent'}
        value={this.state.usuario}
        onChangeText={(usuario) => this.changeUsuario(usuario)}
        />
        <TextInput
        placeholder="Contraseña"
        style={styles.TextInput}
        underlineColorAndroid={'transparent'}
        secureTextEntry={true}
        value={this.state.contraseña}
        onChangeText={(contraseña) => this.changeContraseña(contraseña)}
        />
        <TouchableHighlight onPress={() => this.boton()}>
                <Text style={styles.text_containerr}>¿Olvido su contraseña?</Text>
            </TouchableHighlight>
              <TouchableHighlight style={styles.botton} onPress={() => this.botonPress()}>
                <Text style={styles.text_container}>Iniciar Sesión</Text>
            </TouchableHighlight>
             <Image
        style={{ width: 150, height: 150, alignItems: 'center', marginTop: 25, alignSelf: 'center' }}
          source={require("../assets/logoo.jpeg")}
        />
        </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e84393',
    alignSelf: 'stretch'
  },
  text_container: {
      fontSize: 30,
      color: '#ecf0f1',
      fontFamily:''
  },
   text_containerr: {
      fontSize: 15,
      color: '#ecf0f1'
  },
  botton:{
    alignSelf: 'center',
    alignItems: 'center',
    padding: 20,
    width: 400,
    marginTop: 30,
    borderColor:"#fff",
    borderWidth:1,

  },
  TextInput:{
    alignSelf: 'stretch',
    fontSize: 15,
    height: 40,
    marginTop:30,
    marginBottom: 30,
    color: '#fff',
    borderBottomColor: '#f8f8f8',
    borderBottomWidth: 1,

  },
  textodeprueba:{
        alignSelf: 'stretch',
    fontSize: 50,
    height: 40,
    marginTop:30,
    marginBottom: 30,
    color: '#fff',
    borderBottomColor: '#f8f8f8',
    borderBottomWidth: 1,
  }
});

export default Form;