import React, {Component} from 'react';
import { Text, View, StyleSheet, Touchable, TouchableOpacity } from 'react-native';

class Button extends Component {
    render() {
        const { label, action } = this.props;
        return(
            <TouchableOpacity
            style={styles.btn}
            onPress={action}          
            >
                <Text style={styles.btnText}>{this.props.label}</Text>
            </TouchableOpacity>
        );
    }
}
const styles = StyleSheet.create({
  btn: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ecf0f1'
  },
  btnText: {
    fontSize: 30,
    color: '#2d3436',
    fontWeight: 'bold'
  },
});
export default Button