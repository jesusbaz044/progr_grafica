import React, {Component} from 'react';
import { View, StyleSheet, Text, Button, TouchableHighlight, FlatList, Image, Touchable, TouchableOpacity} from 'react-native';
import Icon from 'react-native-ionicons'
function Pelis({peli, onPress}){
    return(
        <TouchableOpacity onPress={onPress}>
            <View style={styles.home}>
                <Text style={styles.titlee}>{peli.title}</Text>
                <Image style={styles.image} source={{uri: peli.cover}}></Image>
            </View>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    home:{
        backgroundColor: '#e84393',
    },
    image: {
        width: 100,
        height: 100,
        resizeMode: 'cover',
        alignSelf: 'center',
        marginBottom: 10,
        borderWidth: 3,
        borderColor: '#fff'
    },
    titlee:{
        fontWeight:'bold',
        color:'#fff',
        textAlign:'center'
    },
    icon:{
        color: "#000"
    }
})
export default Pelis;
