import * as React from 'react';
import { View, Text, Image,StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Form from './src/view/form' ;
import Home from './src/view/home' ;
import HomeScreen from './src/view/detalle' ;

const Stack = createStackNavigator();
function App() {
  return (
      <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Form" component={Form} options={{title: 'Inicio de Sesion', headerStyle: {backgroundColor: '#e84393'}, headerTintColor: '#fff',           headerTitleStyle: {
            fontWeight: 'bold',
          }, }}/>
        <Stack.Screen name="Home" component={Home} options={{title: 'Home', headerStyle: {backgroundColor: '#e84393'}, headerTintColor: '#fff',           headerTitleStyle: {
            fontWeight: 'bold',
          }, }}/>
            <Stack.Screen name="HomeScreen" component={HomeScreen} options={{title: 'Detalle', headerStyle: {backgroundColor: '#e84393'}, headerTintColor: '#fff',           headerTitleStyle: {
            fontWeight: 'bold',
          }, }}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default App;